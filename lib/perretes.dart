import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_perretes/app_configuration.dart';
import 'package:app_perretes/dog_ceo_client.dart';

///////////////////////////////////////////////////////////////////////////
//
// PerrretesApp

class PerretesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'App de perretes',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MasterDetailContainer(),
    );
  }
}

///////////////////////////////////////////////////////////////////////////
//
// PerretesScaffold

class PerretesScaffold extends StatelessWidget {
  PerretesScaffold({this.title, this.body});

  final String title;
  final Widget body;
  
  @override
  Widget build(BuildContext context) =>
    new Scaffold(
        appBar: new AppBar(
            title: new Text(this.title ?? AppConfiguration.of(context).appTitle),
        ),
        body: this.body,
    );
}

///////////////////////////////////////////////////////////////////////////
//
// MasterDetailContainer

class MasterDetailContainer extends StatelessWidget {

  @override
  Widget build(BuildContext context) =>
     new LayoutBuilder(
        builder: (context, constraints) {
          final double smallestDimension = min(
              constraints.maxWidth,
              constraints.maxHeight,
          );
          final bool useMobileLayout = smallestDimension < 600;
          return new PerretesScaffold(
              body: useMobileLayout ? new PerretesMobileBody() : new PerretesTabletBody(),
          );
        },
    );
}

class PerretesMobileBody extends StatelessWidget {

  @override
  Widget build(BuildContext context) =>
    new PerretesMaster(
        itemSelectedCallback: (DogBreed item) {
            Navigator.of(context).push(
                new MaterialPageRoute(
                    builder: (context) {
                      return new PerretesScaffold(title: item.name, body: new PerretesDetail(breed: item));
                    },
                ),
            );
          },
    );
}

class PerretesTabletBody extends StatefulWidget {
  @override
  _PerretesTabletBodyState createState() => new _PerretesTabletBodyState();
}

class _PerretesTabletBodyState extends State<PerretesTabletBody> {
  DogBreed _selected = null;
  
  @override
  Widget build(BuildContext context) =>
      new Row(
        children: <Widget>[
          new Flexible(
              flex: 1,
              child: new Material(
                 elevation: 4.0,
                 child: new PerretesMaster(
                    selectedItem: _selected,
                    itemSelectedCallback: (DogBreed item) {
                        setState(() {
                            _selected = item;
                        });
                    },
                  ),
              ),
          ),
          new Flexible(
              flex: 3,
              child: _empytOrDetail(),
          )
        ],
      );

  Widget _empytOrDetail() {
    if (_selected == null) {
      return new Center(
        child: new Text('Please select a breed.',
            style: Theme.of(context).textTheme.display1),
      );
    }
    else {
      return new PerretesDetail(
        breed: _selected,
      );
    }
  }
}


///////////////////////////////////////////////////////////////////////////
//
// PerretesMaster

class PerretesMaster extends StatefulWidget {
  PerretesMaster({this.itemSelectedCallback, this.selectedItem}) : super(key: key);

  final ValueChanged<DogBreed> itemSelectedCallback;
  final DogBreed selectedItem;

  @override
    _PerretesMasterState createState() => new _PerretesMasterState();
}


class _PerretesMasterState extends State<PerretesMaster> {
  List<DogBreed> breeds = [];

  void _loadDataAsset() async {
    AssetBundle asset = DefaultAssetBundle.of(context);
    String json = await asset.loadString('data/breeds_list.json');
    Map data = jsonDecode(json);
    // Problemas con la inferencia de tipos
    //List<DogBreed> parsedData = data['message'].keys.map((String name) => new DogBreed(name: name)
    //                                                     ).toList();
    List<DogBreed> parsedData = [];
    for(String name in data['message'].keys) {
      parsedData.add(new DogBreed(name :name));
    }
    setState(() {
      this.breeds = parsedData;
    }); 
  }
  
  @override
  Widget build(BuildContext context) {
    final TextStyle fontStyle = new TextStyle(fontSize: Theme.of(context).textTheme.display1.fontSize);
    
    if (this.breeds.isEmpty) {
      _loadDataAsset();
    }
    return new ListView(
          children: this.breeds.map((breed) => new DogBreedTile(
                  breed: breed,
                  style: fontStyle,
                  selected: breed.name == widget.selectedItem?.name,
                  tapCallback: widget.itemSelectedCallback
          )).toList(),
    );
  }
}

class DogBreedTile extends StatelessWidget {
  const DogBreedTile({this.breed, this.style, this.selected, this.tapCallback}); // TBD: must be non optional

  final DogBreed breed;
  final TextStyle style;
  final bool selected;
  final ValueChanged<DogBreed> tapCallback;

  @override
  Widget build(BuildContext context) {
    return new ListTile(
        title: new Text(
            this.breed.name,
            style: this.style,
        ),
        selected: this.selected,
        onTap: () { this.tapCallback(this.breed); },
    );
  }
}


///////////////////////////////////////////////////////////////////////////
//
// PerretesDetail

class PerretesDetail extends StatefulWidget {
  PerretesDetail({this.breed});

  final DogBreed breed;
  
  @override
  _PerretesDetailState createState() => new _PerretesDetailState();
}


enum _ImageState { initial, loading, loaded, error }

class _PerretesDetailState extends State<PerretesDetail> {

  _ImageState state = _ImageState.initial;
  String imageURL = '';

  @override
  void didUpdateWidget(PerretesDetail oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.breed != this.widget.breed) {
      _loadImage(widget.breed.name);
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle fontStyle = new TextStyle(fontSize: Theme.of(context).textTheme.display1.fontSize);
    if (this.state == _ImageState.initial) {
      _loadImage(widget.breed.name);
    }
    return new Padding(
        padding: new EdgeInsets.all(6.0),
        child:  new SingleChildScrollView(child: new Center(child: new Column(
                children: <Widget>[
                  Text(widget.breed.name, style: fontStyle),
                ] + _buildDetail(fontStyle)),
    )));
  }

  List<Widget> _buildDetail(fontStyle) {
    switch(this.state) {
      case _ImageState.loading:
        return <Widget>[
          new Text("Loading, please wait ..", style: fontStyle),
          new Center(child: new Image.asset("images/snoopy.gif")),
        ];
      case _ImageState.loaded:
        return <Widget>[new Center(child: new Image.network(this.imageURL))];
      case _ImageState.error:
        return <Widget>[
          new Image.asset("images/snoopy-penalty-box.gif"),
          new Text("Network error", style: fontStyle), // TBD: Esto hay que currarselo más
          new RaisedButton(
              child: new Text("Try again"),
              onPressed: () {
                setState(() {
                  this.state = _ImageState.loading;
                  _loadImage(widget.breed.name);
                });    
              },
          ),
        ];
    }
  }
  
  void _loadImage(String name) async {
    setState(() {
      this.state = _ImageState.loading;
    });
    AppConfiguration.of(context).dogCeoClient.loadBreedImageURL(name)    
      .then((String url) {
        if (mounted) {
          setState(() {
            this.state = _ImageState.loaded; // Mentiiiiira
            this.imageURL = url;
          });
        }
      })
      .catchError((e) {
        if (mounted) {
          setState(() { this.state = _ImageState.error; });
        }
      });
  }
}


class DogBreed {
  const DogBreed({this.name});

  final String name;
}