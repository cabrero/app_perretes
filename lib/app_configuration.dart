import 'package:app_perretes/dog_ceo_client.dart';
import 'package:app_perretes/mocks/dog_ceo_client_mock.dart';
import 'package:flutter/material.dart';

enum NetBehaviour { normal, slow, error }

class AppConfiguration extends InheritedWidget {
  final NetBehaviour netBehaviour;
  final Widget child;
  DogCeoClient dogCeoClient;
  String appTitle;

  AppConfiguration({ this.netBehaviour, this.child }) : super(child: child) {
    this.appTitle = "App de perretes";
    switch(netBehaviour) {
      case NetBehaviour.normal:
        this.dogCeoClient = new DogCeoClient();
        break;
      case NetBehaviour.slow:
        this.dogCeoClient = new DogCeoClientMock(MockBehaviour.slow);
        this.appTitle += " (slow net)";
        break;
      default:
        this.dogCeoClient = new DogCeoClientMock(MockBehaviour.error);
        this.appTitle += " (error net)";
    }
  }

  static AppConfiguration of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppConfiguration);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
