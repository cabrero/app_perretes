import 'package:app_perretes/app_configuration.dart';
import 'package:app_perretes/perretes.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new AppConfiguration(
          netBehaviour: NetBehaviour.slow,
          child: new PerretesApp(),
         ),
  );
}
