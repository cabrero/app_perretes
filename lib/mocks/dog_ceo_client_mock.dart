import 'dart:async';
import 'dart:math';

import 'package:app_perretes/dog_ceo_client.dart';

enum MockBehaviour { normal, slow, error }

class DogCeoClientMock implements DogCeoClient {
  DogCeoClientMock(this.behaviour);

  DogCeoClientMock.slow() : behaviour = MockBehaviour.slow { }
  DogCeoClientMock.error() : behaviour = MockBehaviour.error { }

  final MockBehaviour behaviour;
  final DogCeoClient realClient = new DogCeoClient();

  Future<String> loadBreedImageURL(String breed) async {
    switch(behaviour) {
      case MockBehaviour.slow:
        Random rng = new Random();
        return new Future.delayed(
          new Duration(seconds:rng.nextInt(10)),
          () => realClient.loadBreedImageURL(breed),
        );

      case MockBehaviour.error:
        return new Future.delayed(
          new Duration(seconds: 1),
              () => throw "¡Error salvaje aparece!",
        );

      default:
        return realClient.loadBreedImageURL(breed);
    }
  }
}